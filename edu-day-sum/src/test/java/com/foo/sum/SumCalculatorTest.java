package com.foo.sum;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SumCalculatorTest {

    private SumCalculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new SumCalculator();
    }

    @Test
    void calculate() {
        int a = 1;
        int b = 1;
        int result = calculator.calculate(a, b);
        assertEquals(a+b, result);
    }
}