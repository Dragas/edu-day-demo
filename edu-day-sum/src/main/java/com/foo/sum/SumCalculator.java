package com.foo.sum;

import com.foo.api.Calculator;

public class SumCalculator implements Calculator {
    public int calculate(int a, int b) {
        return a + b;
    }

    public String getOperation() {
        return "sum";
    }
}
