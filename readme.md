# Education day demo

Contains a step by step migration and 
play along demo (with exercises) showing 
somewhat real world issues when trying to
deal enter the project modularity.

Not every "pain point" is covered, except
the very basics to stay within the half an
hour presentation limit. In case you are
interested in depth how JPMS works, I suggest
watching [Modular Development with JDK9 by Mark Reinhold/Alan Bateman](https://www.youtube.com/watch?v=V6ycn1-QQV0)
and [Project Jigsaw: Under The Hood](https://www.youtube.com/watch?v=QnMDsI2GbOc).
These are a bit old, and outdated, considering
JPMS related keywords had changed, but the ideas
in how to structure the project, how to migrate
it, and what to expect of JPMS are there.

## Building

Each tag is built using `package` maven goal
```shell script
./mvnw package
```
In case you're building any tags beyond `simple-project`, 
remove `**/*/classes` directory.

## Running

Tags before `modular` are ran by invoking the following
```shell script
java -jar edu-day-runtime/target/edu-day-runtime-1.0-SNAPSHOT.jar 1 1
```
Tags starting with `modular` are ran by invoking the following
```shell script
java -p edu-day-runtime/target:edu-day-runtime/target/dependency -m edu.day.runtime
```

## Troubleshooting

`modules` tag shows the compatibility mode where a 
named module depends on non modular jars. This is
a valid case in JPMS, considering not everyone migrated
(and some are not willing to do it). The compatibility
mode is called "filename modules" or "automatic modules".
Usage of this compatibility mode prevents using jlink
tool, but from my experience this is not relevant.
What is relevant is that IntelliJ incorrectly shows
that these modules are not available and packages
available in them are missing, even though `maven-compiler-plugin`
is perfectly capable of configuring the `javac` tool
to compile the project using modular mode,
and even produces the lovely message not to upload
`edu-day-runtime` module to external repositories,
since it requires filename based modules. This issue
does not seem to be present when pulling non-modular
artifacts from maven central.

Testing is also painful in IntelliJ with JPMS, mainly
because JUnit and other tools require that your test
suites are available for reflection. JPMS by default
does not permit reflection, even on public elements.
Solution to this is to create a new module-info.java
file in `test` folder, and let `maven-jar-plugin` to
repackage the `testJar` with test module descriptor,
rather than real one from `main` sources. JPMS accepts
this solution and goes on its merry way, while IntelliJ
complains that there are multiple module descriptors
for the same module, and breaks the intellisense plugin
to prevent you from indexing everything outside your
current module. Workaround for this is to first write tests,
and only then to create a module descriptor. See 
[Sormuras: Testing in the modular world](https://sormuras.github.io/blog/2018-09-11-testing-in-the-modular-world.html)
for more information