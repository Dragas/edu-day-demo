package com.foo.runtime;

import com.foo.api.Calculator;
import com.foo.sum.SumCalculator;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new SumCalculator();
        int calculated = calculator.calculate(Integer.valueOf(args[0], 10), Integer.valueOf(args[1], 10));
        System.out.printf("Result of %s is %d%n", calculator.getOperation(), calculated);
    }
}
