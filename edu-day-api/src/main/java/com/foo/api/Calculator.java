package com.foo.api;

public interface Calculator {
    int calculate(int a, int b);
    String getOperation();
}
